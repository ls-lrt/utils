#!/bin/bash

>&2 cat <<EOF
 
 R1        R2        R3        R4        R6        R7        
 France 2  BFM TV    Canal+    6Ter      LCP       Chérie 25
 France 3  D8        C+ ciné   Arte      NT1       HD1
 Fr3 local D17       C+ sport  France 5  NRJ 12    L'Équipe 21
 France 4  Gulli     LCI       M6        TMC       Numéro 23
 France Ô  iTÉLÉ     Paris 1   W9        TF1       RMC Découverte
                     Planète +
 http://www.csa.fr/
 http://www.matnt.tdf.fr/
EOF

#T 474166000 8MHz 2/3 NONE QAM64 8k 1/32 NONE

list_multiplex="R1 R2 R3 R4 R6 R7"
first_chan=21
last_chan=60
f_first_chan=474166000
delta_f=8000000
declare -A Multiplex

for m in $list_multiplex; do
    read -p "Canal (${first_chan} - ${last_chan}) pour une chaîne du multiplex $m: " Multiplex[$m]
    echo "#${m}"
    echo "T $((f_first_chan + (Multiplex[$m] - $first_chan) * delta_f)) 8MHz 2/3 NONE QAM64 8k 1/32 NONE"
done

# scan scan.conf > channels.conf
