#!/bin/bash

final_pdf="eca-aerospace.pdf"
old_pdf="eca-aerospace.pdf.old"

set -- `ls -lrth *.pdf | awk '{print $9;}' | tr '\n' ' '`
len=$#

# find the position of $final_pdf in the list
pos=0
for f in $@; do
    pos=$((pos + 1))
    [ "$f" = "$final_pdf" ] && break
done

# exclude all files oldest than $final_pdf
shift $pos

[ $# -eq 0 ] && echo "Rien de nouveau à fusionner" && exit 0

# reverse the list
set -- `ls -lt $@ | awk '{print $9;}' | tr '\n' ' '`

# move the pdf
[ -e "$old_pdf" ] && echo "mv $old_pdf /tmp" && mv "$old_pdf" /tmp
mv "$final_pdf" "$old_pdf"

# merge
echo "pdfunite $@ $old_pdf $final_pdf" && pdfunite $@ $old_pdf $final_pdf
