#!/bin/bash

function defil()
{
    local tput=$1 columns=$2 sleep=$3; shift 3
    local long_txt="$*"

    if [ ${#long_txt} -lt $columns ]; then
	echo -n $long_txt
    else
	if [ $tput -eq 1 ]; then
	    tput sc
	fi

	local start=1 end=$((columns - 3))
	echo -n ${long_txt:$start:$((columns-3))}"..."
	while [ $end -le ${#long_txt} ]; do
	    sleep "$sleep"
	    if [ $tput -eq 1 ]; then
		tput rc
	    else
		echo -en "\e[0K\r"
	    fi
	    echo -n "..."${long_txt:$start:$((columns-3))}
	    start=$((start + 3))
	    end=$((end + 3))
	done
    fi
    echo
}

##
# Pretty print function
# options:
#      -p prompt
#      -n max_number_of_columns
#      -s sleep_time
##
function pprint()
{
    local OPTIND prompt="" n=80 max_len=80 sleep=0.2
    while getopts "n:p:s:" opt
    do
	case $opt in
	    n)
		n=$OPTARG
		;;
	    p)
		prompt=$OPTARG
		;;
	    s)
		sleep=$OPTARG
		;;
	esac
    done
    shift $((OPTIND - 1))
    if [ $n -le $((${#prompt} + 3)) ]; then
	echo "$prompt"
    else
	echo -n "$prompt"
    fi

    if [ $n -gt 3 ]; then
	max_len=$n
    fi

    which tput >/dev/null 2>&1
    if [ $? -eq 0 ]; then
	defil 1 $max_len $sleep $@
    else
	defil 0 $max_len $sleep $@
    fi
}

function _test() {
    echo "#################"
    echo "pprint -p \"> \" -n 80 \"foo bar moo\""
    pprint -p "> " -n 80 "foo bar moo"
    echo "#################"
    echo

    echo "#################"
    echo "pprint -p \"prompt> \" \$(ls -m)"
    pprint -p "prompt> " $(ls -m)
    echo "#################"
    echo

    echo "#################"
    echo "pprint -p \"prompt> \" -n 30 -s 0.1 \$(ls -m)"
    pprint -p "prompt> " -n 30 -s 0.1 $(ls -m)
    echo "#################"
    echo

    echo "#################"
    echo "pprint -p \"prompt> \" -n 0 -s 0.1 \$(ls -m)"
    pprint -p "prompt> " -n 0 -s 0.1 $(ls -m)
    echo "#################"
    echo

    echo "#################"
    echo "pprint -p \"reaaaaaaaaaaaaaaaaaaaaalyyyyyyyyyyyyy long prompt> \" -n 30 -s 0.1 \$(ls -m)"
    pprint -p "reaaaaaaaaaaaaaaaaaaaaalyyyyyyyyyyyyy long prompt> " -n 30 -s 0.1 $(ls -m)
    echo "#################"
    echo
}
