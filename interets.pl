#!/usr/bin/perl -w

use strict;
use Getopt::Long;

sub hline
{
    my $l = shift || 62;
    my $c = shift || "-";
    for (my $i = $l; $i; $i--) {print $c;};
    print "\n";
}

sub help
{
    print "$0 [--capital|-c capital enprunté]
\t[--taux|-t taux d'emprunt]
\t[--duree|-d nombre de mensulatités]
\t[--mensualite|-m montant de la mensualtité]\n";
    exit;
}

# Rn+1 = Rn + t.Rn - M
# Rn = (1 + t)^n . (C - M/t) + M/t
#
# Rn: Somme restante à rembourser
# C : Capital emprunté
# t : Taux de la période
# M : Montant des échéances
# n : Nombre d'échérances

# (1 + t)^N = - M/t . (t / (C.t -M))
# N = [ln(M) - ln(M -Ct)] / ln(1+t)

my ($C, $t, $N, $M);# = @ARGV;

GetOptions ("capital|c=i"     => \$C,
	    "taux|t=f"        => \$t,
	    "duree|d=i"       => \$N,
	    "mensualite|m=i"  => \$M,
	    "help|h!"  => \&help )
    or die("Error in command line arguments\n");

if (!defined $t) {
    print "Taux d'emprunt (pourcentage annuel)? ";
    chomp ($t = <STDIN>);
}
die "Taux non spécifié ou négatif." if (!defined $t || $t < 0);
# taux mensuel
$t /= 100;
$t /= 12;

if (!defined $M) {
    if (!defined $N) {
	print "Durée du prêt (en année)? ";
	chomp ($N = <STDIN>);
	$N *= 12;
    }
    if (!defined $C) {
	print "Capital à emprunter? ";
	chomp ($C = <STDIN>);
    }
    die "Capital non spécifié ou négatif." if (!defined $C || $C < 0);
    $M = $t * $C * ((1 + $t)**$N) / ( ((1 + $t)**$N) - 1);
}

if ($C < 0) {
    if (!defined $N) {
	print "Durée du prêt (en année)? ";
	chomp ($N = <STDIN>);
    }
    $N *= 12;
    $C = ($M / $t) * ((((1 + $t)**$N) - 1)/(((1 + $t)**$N)));
}

my $Rn = $C;
my $n = 1;
my ($R, $i, $ic, $RR) = 0;
my $delta = 0.01;
my $save_M = $M;
die "Intérêts supérieurs à la mensualité.\n" if ($M <= ($t * $Rn));

for (;;$n++) {
    if (($Rn + ($t * $Rn)) < $M) {
	$M = $Rn + ($t * $Rn);
    }

    $i += ($t * $Rn);
    $ic += ($t * $Rn);
    $R += $M - ($t * $Rn);
    $RR += $M - ($t * $Rn);

    printf "%3d %20f %20f %20f\n", $n, $Rn, ($t * $Rn), $R;

    $Rn += ($t * $Rn) - $M;

    if (!($n%12) || $Rn <= $delta) {
	hline(66, "=");
	printf "                         %20f %20f\n", $ic, $RR;
	printf "%3d %20f %20f %20f\n\n", $n / 12, $Rn, $i, $R;
	$ic = 0;
	$RR = 0;
    }
    last if ($Rn <= $delta || (defined $N && $n > $N));
}
$M = $save_M;

hline();
print " Emprunté              : $C\n";
print " Mensualités           : $M\n";
print " Nombre de mensualités : $n (".($n / 12)." ans)\n";
print " Taux mensuel          : ".($t * 100)."% (".($t * 100 * 12)."% / an)\n";
print " Coût de l'emprunt     : $i\n";
hline();
