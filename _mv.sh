#!/bin/sh

# Move the last item of $FROM (default ./) into $TO (default ./)
_mv () { FROM=$1; TO=$2; [ "X$FROM" = "X" ] && FROM="./"; [ "X$TO"   = "X" ] && TO="./"; set -- $(ls -t "$FROM"); mv "$FROM/$1" "$TO"; }
