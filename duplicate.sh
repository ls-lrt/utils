#!/bin/bash

LIST=$1
shift

if [ -e "$LIST" ]; then
    read -p "Warning, $LIST exists, removing it? [y/N] " res
    if [ "$res" != "y" ]; then
	exit 1
    fi
    rm -f "$LIST"
fi

declare -A pids
for i in $@
do
    md5sum "$i" >> /tmp/list&
    pids["$i"]=$!
done

echo -n "Waiting for completion of "${#pids[@]}" jobs "
for pid in ${pids[*]};
do
    wait $pid && echo -n "."
done
echo " done!"

declare -A a
while read sum name; do
    if [ -z ${a["_$sum"]} ]; then
	a["_$sum"]=0
	eval "_$sum=()"
    fi
    a["_$sum"]=$((${a["_$sum"]} + 1))
    eval "_${sum}+=(${name})"
done <"$LIST"

for i in ${!a[*]}
do
    n=$(eval "echo \${#$i[@]}")
    if [ $n -gt 1 ]; then
	eval "echo $i: \${$i[@]}"
    fi
done
