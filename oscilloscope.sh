#!/bin/sh

# Gnuplot wrapper to plot some data comming from stdin, quit with C^c
# Test command:
# i=0; while ((1)); do echo $((i)) $((i*i)) $((i*i*i)); i=$((i+1)); sleep 0.2; done | ./oscilloscope.sh x x^2 x^3

TMP_FILE=/tmp/oscilloscope_${RANDOM}
DAT_FILE=${TMP_FILE}.dat
PLO_FILE=${TMP_FILE}_plot.gnu
PAUSE_S=1
KEEP_FILES=

function cleanup { [ -z "${KEEP_FILES}" ] && rm ${DAT_FILE} ${PLO_FILE}; }
function count   { echo $#; }
function usage   { echo "Usage: $0 [−k] [-p n] [name1 name2 ...]"; exit 2; }

trap cleanup SIGINT SIGTERM

while getopts hkp: name
do
    case $name in
	k) KEEP_FILES=1
	   shift;;
	p) PAUSE_S=${OPTARG}
	   shift 2;;
	h|?) usage;;
    esac
done

format=
while read values; do
    echo $values >> ${DAT_FILE}
    if [ -z "${format}" ]; then
	for i in `seq 1 $(count ${values})`; do
	    title=${i}
	    if [ ! -z "$1" ]; then
		title="$1"; shift
	    fi
	    format="${format} '${DAT_FILE}' using ${i} title \"${title}\" with lines,"
	done
	echo "set term wxt title 'oscilloscope'; plot ${format}; pause ${PAUSE_S}; reread" > ${PLO_FILE}
	gnuplot ${PLO_FILE} &>/dev/null &
    fi
done 2>/dev/null
