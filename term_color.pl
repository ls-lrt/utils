#!/usr/bin/perl -w

use strict;

# From wikipedia:
# In 256-color mode (ESC[38;5;<fgcode>m and ESC[48;5;<bgcode>m), the color-codes are the following:
#
# 0x00-0x07:  standard colors (as in ESC [ 30–37 m)
# 0x08-0x0F:  high intensity colors (as in ESC [ 90–97 m)
# 0x10-0xE7:  6 × 6 × 6 = 216 colors: 16 + 36 × r + 6 × g + b (0 ≤ r, g, b ≤ 5)
# 0xE8-0xFF:  grayscale from black to white in 24 steps

my ($r, $g, $b, $c) = 0;

print "\x1b[0m\n ";
for ($c = -1; $c < 16; $c++) {
    if ($c < 0) {
	for (my $i = 0; $i < 16; $i++) { printf "%2d ", $i; };
	print "\x1b[0m\n ";
    } else {
	print "\x1b[48;5;${c}m   ";
    }
}

print "\x1b[0m\n\n";
for ($b = -1; $b < 6; $b++) {
    for ($r = 0; $r < 6; $r++) {
	if ($b < 0) {
	    print "   +";
	} else {
	    printf "%3d ", 16 + (36 * $r) + $b;
	}
	for ($g = 0; $g < 6; $g++) {
	    if ($b < 0) {
		printf "%2d ", 6 * $g;
	    } else {
		$c = 16 + 36 * $r + 6 * $g + $b;
		print "\x1b[48;5;${c}m   ";
	    }
	}
	print "\x1b[0m ";
    }
    print "\n";
}

my ($in, $color);
my %text;

while (1) {
    print "\n Color to use? ";
    $color = <STDIN>;
    last if (!defined $color);
    chomp $color;
    die "Error: The color should be in [0-$c]\n" if ($color > $c || $color < 0);

    print " Enter some text to test color or ^d to quit: \x1b[38;5;${color}m";
    $in = <STDIN>;
    print "\x1b[0m";
    last if (!defined $in);
    chomp $in;
    for (my $up = 0; $up < 3; $up++) { print "\x1b[F\x1b[K"; };
    $text{$color} = $in;
};

print "\n Here is some sample of what you have typed:\n";
for my $k (keys %text) {
    print "\\x1b[38;5;${k}m\x1b[38;5;${k}m $text{$k} \x1b[0m\\x1b[0m\n";
}
