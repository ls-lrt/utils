#!/bin/bash

DIR=${1:-./}
files=$(find "$DIR" -type f)
declare -A numeros
declare -A etoiles

function __rand() {
    2>/dev/null dd if=/dev/urandom of=/dev/stdout count=1
}

function _rand() {
    f=$(echo "$files" | sort -R | head -1)
    sha256sum "$f"
}

function rand() {
    n=0
    while [ $n -lt 1 ]; do
	n=$(($(_rand | tr -d -c '0123456789' | sed 's/^0*//') % $1))
    done
    echo $n
}

function t_numero() {
    x=0
    while [ $x -eq 0 ]; do
	x=$(rand 51)
	for b in ${numeros[@]}; do
	    if [ $x -eq $b ]; then
		x=0
		break
	    fi
	done
    done
    numeros[$1]=$x
}

function t_etoile() {
    x=0
    while [ $x -eq 0 ]; do
	x=$(rand 12)
	for b in ${etoiles[@]}; do
	    if [ $x -eq $b ]; then
		x=0
		break
	    fi
	done
    done
    etoiles[$1]=$x
}

for n in `seq 0 4`; do
    t_numero $n
done

for n in `seq 0 1`; do
    t_etoile $n
done


for b in ${numeros[@]}; do
    echo -n " $b"
done
for e in ${etoiles[@]}; do
    echo -n " *$e"
done
echo ""
