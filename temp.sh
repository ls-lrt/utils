#!/bin/bash

# Assuming you have smething like this that logs your thermal info
# using syslog facilities:
# */5 * * * * /usr/bin/sensors | /usr/bin/logger -p daemon.info

# General options
HOST=berbouche
USER=leo
LOG_FILE=/var/log/daemon.log
TMP_FILE=temp.tmp
DATE_FILE=date.tmp
TMP_DIR=/tmp/temp
# Archive options
OUT_ARCHIVE=/home/www/public/temperature
ARCHIVE_FMT="%Y-%m-%d_%H:%M:%S"
# Plotting options
OUT_SVG=/home/www/public/t.svg
SVG_SIZE="1360,768"
Y_RANGE="[:]"
TIME_FMT="\"%b %d - %H:%M\""

##
# Helper functions
##
TIC=0

function tic() {
    TIC=`date +"%s"`
    echo -n ${1:-"tic..."}
}

function toc() {
    DELTA_T=$((`date +"%s"` - TIC))
    echo ${1:-"toc"} " ($DELTA_T s)"
}

##
# Init
##
declare -A filter_out
filter_out=(["$TMP_FILE"]=1 ["$DATE_FILE"]=1)
for i in $@; do
    filter_out+=(["$i"]=1)
done

if [ ! -e "$TMP_DIR" ]; then
    mkdir "$TMP_DIR"
else
    rm -rf "$TMP_DIR"/*
fi

##
# Gathering and first cleanup of logs
##
tic "Scanning parsing files..."
for logfile in `ls -rt ${LOG_FILE}*`; do
    case `basename "$logfile"` in
	*.gz)
	    zcat "$logfile";;
	*)
	    cat "$logfile";;
    esac
done | grep -e "${HOST} ${USER}.*°C.*" > "$TMP_DIR"/"$TMP_FILE"
toc "done"

##
# Extracting info
##
tic "Extracting info..."
awk -F: '{print $4 $5;}' "$TMP_DIR"/"$TMP_FILE"  |  sed 's/ //g;s/+/ /g;s/°C.*//g' | while read file_name t; do
    echo $t >> "$TMP_DIR"/"$file_name"
done
toc "done"

##
# Generating time file
##
tic "Generating time file..."
# Compute the first year
LAST_JAN=`awk '{print $1;}' "$TMP_DIR"/"$TMP_FILE" | uniq | grep -n Jan | tail -1 | awk -F: '{print $1}'`
START_YEAR=$((`date +"%Y"` - (LAST_JAN/12 + (LAST_JAN%12 > 1 ? 1 : 0))))

START=$(date +"%s" -d "`head -1 "$TMP_DIR"/"$TMP_FILE" | awk '{print $1" "$2" "$3;}'` $START_YEAR")
STOP=$(date +"%s" -d "`tail -1 "$TMP_DIR"/"$TMP_FILE" | awk '{print $1" "$2" "$3;}'`")

# Select a data file not in filter_out list to get the number of data points
N_LINES=0
for f in "$TMP_DIR"/*; do
    name=`basename "$f"`
    if [ ! ${filter_out[$name]} ]; then
	N_LINES=`cat "$f" | wc -l`
	break
    fi
done

DELTA=$(((STOP - START) / N_LINES))

# Ensure that we will have the correct number of dates
STOP=$((START + (N_LINES - 1) * DELTA))

for i in `seq $START $DELTA $STOP`; do
    echo $i >> "$TMP_DIR"/"$DATE_FILE"
done

for f in "$TMP_DIR"/*; do
    file_name=`basename $f`
    if [ ! ${filter_out[$file_name]} ]; then
	paste -d ' ' "$TMP_DIR"/"$DATE_FILE" "$TMP_DIR"/"$file_name" > "$TMP_DIR"/temp_date
	mv "$TMP_DIR"/temp_date "$TMP_DIR"/"$file_name"
    fi
done
toc "done"

##
# Plotting
##
tic "Plotting..."
PLOT="set terminal svg size ${SVG_SIZE};set output '${OUT_SVG}';"
PLOT+="set xdata time;set timefmt \"%s\";set xtics format ${TIME_FMT} rotate by 45 right;"
PLOT+="set yrange ${Y_RANGE};"
PLOT+="set title \"`date`\";"
PLOT+="plot "
for data_file in "$TMP_DIR"/*; do
    title=`basename $data_file`
    if [ ! ${filter_out[$title]} ]; then
	PLOT+="\"${data_file}\" using 1:2 with lines title \"${title}\", "
    fi
done
PLOT+=";"
echo "$PLOT" | gnuplot
toc "done!"

##
# Saving data
##
ARCHIVE_NAME=`date +"$ARCHIVE_FMT"`
mkdir -p "$OUT_ARCHIVE"/"$ARCHIVE_NAME"
for data_file in "$TMP_DIR"/*; do
    name=`basename $data_file`
    if [ ! ${filter_out[$name]} ]; then
	cp $data_file "$OUT_ARCHIVE"/"$ARCHIVE_NAME"
    fi
done
gzip -c "$TMP_DIR"/"$TMP_FILE" > "$OUT_ARCHIVE"/"$ARCHIVE_NAME"/temp.tmp.gz
echo "$PLOT" | tr ';' '\n' > "$OUT_ARCHIVE"/"$ARCHIVE_NAME"/plot.gnu
cp "$OUT_SVG" "$OUT_ARCHIVE"/"$ARCHIVE_NAME"
